package com.axlbayu.englishfirst;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.axlbayu.englishfirst.db.Favorit;

import java.util.List;

public class FavoritAdapter extends RecyclerView.Adapter<FavoritAdapter.ViewHolder> {
    public Context context;
    public List<Favorit> favoritList;

    private FavoritAdapter.OnItemClickCallback onItemClickCallback;


    void setOnItemClickCallback(FavoritAdapter.OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public FavoritAdapter(Context context, List<Favorit> favoritList) {
        this.context = context;
        this.favoritList = favoritList;
    }

    @NonNull
    @Override
    public FavoritAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_adapter_favorite,parent,false);
        context = parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoritAdapter.ViewHolder holder, int position) {
        final Favorit favorit = favoritList.get(position);
        holder.judul.setText(favorit.getJudul());
        holder.author.setText(favorit.getAuthor());
        holder.konten.setText(favorit.getArtikel());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Favorit favorit1 = favoritList.get(holder.getAdapterPosition());
                onItemClickCallback.klik(favorit1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return favoritList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView judul, author, konten;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.tv_list_title_fav);
            author = itemView.findViewById(R.id.tv_list_author_fav);
            konten = itemView.findViewById(R.id.tv_list_article_fav);

        }
    }

    public interface OnItemClickCallback{
        void klik(Favorit data);
    }
}
