package com.axlbayu.englishfirst.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FavoritDAO {
    @Query("SELECT * FROM favorit")
    List<Favorit> getAll();

    //Example Custum Query
    @Query("SELECT * FROM favorit WHERE judul LIKE :judul ")
    Favorit findByName(String judul);

    @Insert
    void insertAll(Favorit data);

    @Delete
    public void deleteUsers(Favorit... users);

}
