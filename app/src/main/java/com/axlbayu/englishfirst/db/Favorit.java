package com.axlbayu.englishfirst.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "favorit")
public class Favorit {

    @PrimaryKey(autoGenerate = true)
    int id;

    @ColumnInfo(name = "judul")
    String judul;

    @ColumnInfo(name = "author")
    String author;

    @ColumnInfo(name = "artikel")
    String artikel;

    public Favorit(int id, String judul, String author, String artikel) {
        this.id = id;
        this.judul = judul;
        this.author = author;
        this.artikel = artikel;
    }

    @Ignore
    public Favorit(String judul, String author, String artikel) {
        this.judul = judul;
        this.author = author;
        this.artikel = artikel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getArtikel() {
        return artikel;
    }

    public void setArtikel(String artikel) {
        this.artikel = artikel;
    }
}
