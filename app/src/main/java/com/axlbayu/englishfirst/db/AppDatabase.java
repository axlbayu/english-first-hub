package com.axlbayu.englishfirst.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Favorit.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract FavoritDAO favoritDAO();
}
