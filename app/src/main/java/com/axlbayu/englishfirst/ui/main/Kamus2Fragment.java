package com.axlbayu.englishfirst.ui.main;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.axlbayu.englishfirst.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Kamus2Fragment extends Fragment {


    public Kamus2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_kamus2, container, false);
    }

}
