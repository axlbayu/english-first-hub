package com.axlbayu.englishfirst;

import com.google.firebase.firestore.Exclude;

import io.reactivex.annotations.NonNull;

public class ArticlePostId {

    @Exclude
    public String ArticlePostId;

    public <T extends com.axlbayu.englishfirst.ArticlePostId>T withId(@NonNull final String id){
        this.ArticlePostId = id;
        return (T) this;
    }


}
