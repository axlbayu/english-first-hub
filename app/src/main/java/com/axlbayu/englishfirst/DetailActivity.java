package com.axlbayu.englishfirst;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_GAMBAR = "extra_gambar";
    public static final String EXTRA_THUMB = "extra_thumb";
    public static final String EXTRA_JUDUL = "extra_judul";
    public static final String EXTRA_DESC = "extra_desc";
    public static final String EXTRA_AUTHOR = "extra_author";

    DatabaseHelper myDb;
    private TextView tvDetailTitle, tvDetailAuthor, tvDetailArticle;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_view);

        setTitle("Article Detail");

        myDb = new DatabaseHelper(this);
        tvDetailTitle = findViewById(R.id.tv_detail_title);
        tvDetailAuthor = findViewById(R.id.tv_detail_author);
        tvDetailArticle = findViewById(R.id.tv_detail_article);
        imageView = findViewById(R.id.imageView2);

        String gambarArticle = getIntent().getStringExtra(EXTRA_GAMBAR);
        String thumbArticle = getIntent().getStringExtra(EXTRA_THUMB);
        String judulArticle = getIntent().getStringExtra(EXTRA_JUDUL);
        String deskripsiArticle = getIntent().getStringExtra(EXTRA_DESC);
        String authorArticle = getIntent().getStringExtra(EXTRA_AUTHOR);

        RequestOptions requestOptions = new RequestOptions();

        requestOptions.placeholder(R.drawable.user_male);
        Glide.with(DetailActivity.this).applyDefaultRequestOptions(requestOptions).load(gambarArticle).thumbnail(
                Glide.with(DetailActivity.this).load(thumbArticle)
        ).into(imageView);


        tvDetailTitle.setText(judulArticle);
        tvDetailAuthor.setText(authorArticle);
        tvDetailArticle.setText(deskripsiArticle);
    }
}
