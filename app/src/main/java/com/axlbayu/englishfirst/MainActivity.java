package com.axlbayu.englishfirst;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private Button btn_logout, btnFav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Creative Article");

        firebaseAuth = FirebaseAuth.getInstance();
        btn_logout = findViewById(R.id.btn_keluar);
        btnFav = findViewById(R.id.button3);

        btnFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, FavoritActivity.class));
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firebaseAuth.signOut();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
            }
        });
    }

    public void clickList(View view) {
        startActivity(new Intent(this, ListActivity.class));
    }

    public void clickCreate(View view) {
        startActivity(new Intent(this, CreateActivity.class));
    }
}
