package com.axlbayu.englishfirst;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.axlbayu.englishfirst.db.AppDatabase;
import com.axlbayu.englishfirst.db.Favorit;

import java.util.ArrayList;
import java.util.List;

public class FavoritActivity extends AppCompatActivity {
    List<Favorit> favoritList = new ArrayList<>();
    private FavoritAdapter favoritAdapter;
    private RecyclerView rv_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorit);

        rv_adapter = findViewById(R.id.rv_favorit);


        fetchDataFromRoom();
        initRecyclerView();
        setAdapter();

    }

    private void setAdapter() {
        rv_adapter.setAdapter(favoritAdapter);
    }

    private void initRecyclerView() {
        rv_adapter.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv_adapter.setLayoutManager(llm);
        favoritAdapter = new FavoritAdapter(this,favoritList);
    }

    private void fetchDataFromRoom() {
        AppDatabase db = Room.databaseBuilder(getApplicationContext(), AppDatabase.class,"favorit").allowMainThreadQueries().build();
        favoritList = db.favoritDAO().getAll();

        //just checking data from db
        for (int i = 0 ;i <favoritList.size();i++){
            Log.e("Aplikasi",favoritList.get(i).getJudul()+i);
            Log.e("Aplikasi",favoritList.get(i).getAuthor()+i);
            Log.e("Aplikasi",favoritList.get(i).getArtikel()+i);
        }
    }
}
