package com.axlbayu.englishfirst.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.axlbayu.englishfirst.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Kamus1Fragment extends Fragment {


    public Kamus1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate k layout for this fragment
        return inflater.inflate(R.layout.fragment_kamus, container, false);
    }

}
