package com.axlbayu.englishfirst;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import id.zelory.compressor.Compressor;

public class CreateActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    private EditText etTitle, etAuthor, etArticle;
    private ImageView imageView;
    private Button post;
    private Uri postImageUri = null;
    private StorageReference storageReference;
    private FirebaseFirestore firebaseFirestore;
    private Bitmap compressedImageFile;
    private ProgressBar progressBar;
    private String current_user_id;
    private FirebaseAuth firebaseAuth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_create);

        setTitle("Create Article");

        storageReference = FirebaseStorage.getInstance().getReference();
        firebaseFirestore = FirebaseFirestore.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        current_user_id = firebaseAuth.getCurrentUser().getUid();


//        myDb = new DatabaseHelper(this);
        imageView = findViewById(R.id.imageView4);
        etTitle = findViewById(R.id.edt_input_title);
        etAuthor = findViewById(R.id.edt_input_author);
        etArticle = findViewById(R.id.edt_input_article);
        post = findViewById(R.id.btn_post);
        progressBar = findViewById(R.id.progressBar);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMinCropResultSize(512,512)
                        .setAspectRatio(1,1)
                        .start(CreateActivity.this);
            }
        });

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String judul = etTitle.getText().toString();
                final String author = etAuthor.getText().toString();
                final String artikel = etArticle.getText().toString();

                if (!TextUtils.isEmpty(judul)&& !TextUtils.isEmpty(author)&& !TextUtils.isEmpty(artikel) && postImageUri != null) {
                    progressBar.setVisibility(View.VISIBLE);

                    final String randomName = UUID.randomUUID().toString();

                    final StorageReference filePath = storageReference.child("post_images").child(randomName);
                    filePath.putFile(postImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()){
                                throw task.getException();
                            }
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull final Task<Uri> task) {
                            if (task.isSuccessful()){

                                File newImageFile = new File(postImageUri.getPath());

                                try {
                                    compressedImageFile = new Compressor(CreateActivity.this).
                                            setMaxHeight(100)
                                            .setMaxWidth(100)
                                            .setQuality(2)
                                            .compressToBitmap(newImageFile);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                compressedImageFile.compress(Bitmap.CompressFormat.JPEG,100,baos);
                                byte[] thumbData = baos.toByteArray();

                                UploadTask uploadTask = storageReference.child("post_images/thumbs").child(randomName + ".jpg").
                                        putBytes(thumbData);
                                final Uri downloadUri = task.getResult();
                                final String downloadtextUri = downloadUri.toString();
                                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                                        Task<Uri> uriTask = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                                        String downloadThumbUri = uriTask.toString();
                                        Map<String,Object> postMap = new HashMap<>();
                                        postMap.put("image_url",downloadtextUri);
                                        postMap.put("thumb", downloadThumbUri);
                                        postMap.put("judul",judul);
                                        postMap.put("artikel",artikel);
                                        postMap.put("author",author);
                                        postMap.put("user_id",current_user_id);
                                        postMap.put("timestamp", FieldValue.serverTimestamp());

                                        firebaseFirestore.collection("Posts").add(postMap).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentReference> task) {

                                                if (task.isSuccessful()){

                                                    Toast.makeText(CreateActivity.this,"Artikel telah ditambahkan",Toast.LENGTH_LONG).show();
                                                    Intent mainIntent = new Intent(CreateActivity.this, MainActivity.class);
                                                    startActivity(mainIntent);
                                                    finish();

                                                }else {

                                                    Toast.makeText(CreateActivity.this, "Gagal Membuat Artikel", Toast.LENGTH_LONG).show();



                                                }
                                                progressBar.setVisibility(View.INVISIBLE);

                                            }
                                        });


                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        //Error handling
                                        Log.e("cek", e.getMessage());
                                    }
                                });



                            }else {
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        }
                    });

                }else{
                    Toast.makeText(CreateActivity.this, "Isi semua form", Toast.LENGTH_LONG).show();
                }
            }

        });


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                postImageUri = result.getUri();
                imageView.setImageURI(postImageUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

//    public void clickPost(View view) {
//        String title = etTitle.getText().toString();
//        String author = etAuthor.getText().toString();
//        String article = etArticle.getText().toString();
//
//        if (title.equals("") || author.equals("") || article.equals("")) {
//            Toast.makeText(this, "Mohon isi data secara lengkap", Toast.LENGTH_SHORT).show();
//        } else {
//            boolean insert = myDb.insertData(title, author, article);
//            if (insert){
//                etTitle.setText("");
//                etAuthor.setText("");
//                etArticle.setText("");
//                Toast.makeText(this, "Artikel berhasil di tambahkan", Toast.LENGTH_SHORT).show();
//            } else{
//                Toast.makeText(this, "Artikel gagal di tambahkan", Toast.LENGTH_SHORT).show();
//            }
//        }
//    }
}
