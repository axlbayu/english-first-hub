package com.axlbayu.englishfirst;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.axlbayu.englishfirst.db.AppDatabase;
import com.axlbayu.englishfirst.db.Favorit;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    public List<ArticlePost> article_list;
    public Context context;

    private OnItemClickCallback onItemClickCallback;


    void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    private FirebaseFirestore firebaseFirestore;

    public ListAdapter(List<ArticlePost> article_list) {
        this.article_list = article_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_adapter,viewGroup,false);

        context = viewGroup.getContext();
        firebaseFirestore = FirebaseFirestore.getInstance();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int position) {

        final String judul_data = article_list.get(position).getJudul();
        viewHolder.setJudulText(judul_data);
        final String desc_data = article_list.get(position).getArtikel();
        viewHolder.setDescText(desc_data);
        final String author_data = article_list.get(position).getAuthor();
        viewHolder.setAuthor(author_data);


        //Likes Feature
        viewHolder.blogLikeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                //favoriteTodo
                insert(judul_data,desc_data,author_data);
                viewHolder.blogLikeBtn.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_red_24dp));
                Toast.makeText(context, "Favorite ditambahkan", Toast.LENGTH_SHORT).show();

            }
        });



        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArticlePost articlePost = article_list.get(viewHolder.getAdapterPosition());
                onItemClickCallback.klik(articlePost);
            }
        });


    }

    private void insert(final String judul, final String author, final String konten) {
        Favorit data = new Favorit(judul, author, konten);
        AppDatabase db = Room.databaseBuilder(context, AppDatabase.class,"favorit").allowMainThreadQueries().build();
        db.favoritDAO().insertAll(data);

    }

    @Override
    public int getItemCount() {
        return article_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private View mView;
        private TextView descView;
        private TextView judulView;
        private TextView authorView;

        private ImageButton blogLikeBtn;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mView = itemView;

            blogLikeBtn = mView.findViewById(R.id.imageButton);


        }

        public void setJudulText(String judulText){

            judulView = mView.findViewById(R.id.tv_list_title);
            judulView.setText(judulText);

        }

        public void setDescText(String descText){

            descView = mView.findViewById(R.id.tv_list_article);
            descView.setText(descText);

        }

        public void setAuthor(String author){

            authorView = mView.findViewById(R.id.tv_list_author);
            authorView.setText(author);

        }

    }
    public interface OnItemClickCallback{
        void klik(ArticlePost data);
    }

    //
}
