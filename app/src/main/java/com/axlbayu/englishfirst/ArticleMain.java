package com.axlbayu.englishfirst;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class ArticleMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle("Creative Article");
    }

    public void clickList(View view) {
        startActivity(new Intent(this, ListActivity.class));
    }

    public void clickCreate(View view) {
        startActivity(new Intent(this, CreateActivity.class));
    }
}
