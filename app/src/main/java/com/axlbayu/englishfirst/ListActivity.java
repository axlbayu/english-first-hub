package com.axlbayu.englishfirst;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ListActivity extends AppCompatActivity {
    private List<ArticlePost> article_list;
    private List<Users> users_list;

    private FirebaseAuth firebaseAuth;

    private FirebaseFirestore firebaseFirestore;
    private ListAdapter listAdapter;


    private DocumentSnapshot lastVisible;
    private Boolean isFirstPageFirstLoad = true;

    private RecyclerView rvListArticle;
//    DatabaseHelper myDb;
//    private ArrayList<Article> articles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_list);

        firebaseAuth = FirebaseAuth.getInstance();
        article_list = new ArrayList<>();
        users_list = new ArrayList<>();


        setTitle("Article List");
        rvListArticle = findViewById(R.id.rv_list_article);
//        myDb = new DatabaseHelper(this);

//        getDataArticle();
        listAdapter = new ListAdapter(article_list);
        rvListArticle.setLayoutManager(new LinearLayoutManager(this));
        rvListArticle.setAdapter(listAdapter);

        listAdapter.setOnItemClickCallback(new ListAdapter.OnItemClickCallback() {
            @Override
            public void klik(ArticlePost data) {
                showSelectedData(data);
            }
        });

        if (firebaseAuth.getCurrentUser() != null){

            firebaseFirestore = FirebaseFirestore.getInstance();

            rvListArticle.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    boolean reacheBottom = !recyclerView.canScrollVertically(1);
                    if (reacheBottom){
                        loadPost();
                    }
                }
            });

            Query firstQuery = firebaseFirestore.collection("Posts").orderBy("timestamp", Query.Direction.ASCENDING).limit(30);
            firstQuery.addSnapshotListener(new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (e == null) {
                        if (!queryDocumentSnapshots.isEmpty()) {
                            if (isFirstPageFirstLoad) {
                                lastVisible = queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size() - 1);
                                article_list.clear();
                                users_list.clear();
                            }

                            for (DocumentChange documentChange : queryDocumentSnapshots.getDocumentChanges()) {
                                if (documentChange.getType() == DocumentChange.Type.ADDED) {
                                    String articlePostId = documentChange.getDocument().getId();
                                    final ArticlePost articlePost = documentChange.getDocument().toObject(ArticlePost.class).withId(articlePostId);

                                    String userId = documentChange.getDocument().getString("user_id");

                                    firebaseFirestore.collection("Users").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                            if (task.isSuccessful()) {
                                                Users users = task.getResult().toObject(Users.class);
                                                if (isFirstPageFirstLoad) {

                                                    users_list.add(users);
                                                    article_list.add(articlePost);
                                                } else {
                                                    users_list.add(0, users);
                                                    article_list.add(0, articlePost);
                                                }

                                                listAdapter.notifyDataSetChanged();
                                            }
                                        }
                                    });
                                }
                                isFirstPageFirstLoad = false;
                            }
                        }
                    }
                }
            });
        }


    }

    private void showSelectedData(ArticlePost data) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_GAMBAR,data.getImage_url());
        intent.putExtra(DetailActivity.EXTRA_THUMB,data.getThumb());
        intent.putExtra(DetailActivity.EXTRA_JUDUL, data.getJudul());
        intent.putExtra(DetailActivity.EXTRA_DESC, data.getArtikel());
        intent.putExtra(DetailActivity.EXTRA_AUTHOR, data.getAuthor());
        startActivity(intent);
    }

    private void loadPost() {
        if (firebaseAuth.getCurrentUser() != null){
            Query nextQuery = firebaseFirestore.collection("Posts").
                    orderBy("timestamp", Query.Direction.ASCENDING)
                    .startAfter(lastVisible).limit(30);

            nextQuery.addSnapshotListener(this, new EventListener<QuerySnapshot>() {
                @Override
                public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                    if (!queryDocumentSnapshots.isEmpty()){
                        lastVisible = queryDocumentSnapshots.getDocuments().get(queryDocumentSnapshots.size() - 1);
                        for (DocumentChange doc : queryDocumentSnapshots.getDocumentChanges()){
                            if (doc.getType() == DocumentChange.Type.ADDED){
                                String articlePostId = doc.getDocument().getId();
                                final ArticlePost resepPost = doc.getDocument().toObject(ArticlePost.class).withId(articlePostId);
                                String userId = doc.getDocument().getString("user_id");

                                firebaseFirestore.collection("Users").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                        if (task.isSuccessful()){
                                            Users users = task.getResult().toObject(Users.class);

                                            users_list.add(users);
                                            article_list.add(resepPost);


                                            listAdapter.notifyDataSetChanged();
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    }

//    private void getDataArticle() {
//        Cursor cursor = myDb.getAllData();
//
//        while (cursor.moveToNext()){
//            Article article = new Article();
//            article.setId(cursor.getInt(0));
//            article.setTitle(cursor.getString(1));
//            article.setAuthor(cursor.getString(2));
//            article.setArticle(cursor.getString(3));
//
//            articles.add(article);
//        }
//    }
}
