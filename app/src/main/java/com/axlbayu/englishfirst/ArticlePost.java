package com.axlbayu.englishfirst;

import java.util.Date;

public class ArticlePost extends com.axlbayu.englishfirst.ArticlePostId {
    public String image_url,judul,author,artikel,thumb;
    public Date timestamp;

    public ArticlePost() {
    }

    public ArticlePost(String image_url, String judul, String author, String artikel, String thumb, Date timestamp) {
        this.image_url = image_url;
        this.judul = judul;
        this.author = author;
        this.artikel = artikel;
        this.thumb = thumb;
        this.timestamp = timestamp;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getArtikel() {
        return artikel;
    }

    public void setKonten(String artikel) {
        this.artikel = artikel;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
